# Everything here is WorkInProgress and made to be discussed and changed.   


- This repository contains new infos about the future TiddlyWiki Community [Governance Model](./governance/governance-model.md)
- It contains the [Community Guidlines](./guidelines/community-guidelines.md)

 
# Overview

A few things to do before jumping into the repositories.

## Join the Community

Joining allows you to contribute code, participate in the forums, report and track bugs, and receive updates on the latest software developments.

Have a look at the [Feature Request](https://gitlab.com/tiddlywiki.org/feature-request/issues?sort=weight&state=opened) section, or a different view of the same thing the [Feature Board](https://gitlab.com/tiddlywiki.org/feature-request/boards?=)

## Get familiar with the Components

Check out [our repositories here](https://gitlab.com/tiddlywiki.org) to review our components and README files. If you are brand new to coding and/or to the platform, check out [Requirements.](./governance/requirements.md)

## Discuss Your Idea

We strongly suggest you check the [feature-request section](https://gitlab.com/tiddlywiki.org/feature-request/issues?sort=popularity&state=opened) and post your ideas before you start updating components. 
First of all, you can find out if someone else is working on a similar idea. 
Furthermore, community members can help you refine your concept.

## Review the Governance Model

Follow our process for how to get your contribution accepted as detailed under our [Governance Model](./governance/governance-model.md).

# Licensing

 - A big portion of the "text content" here, will be borrowed from webosose.org and adjusted to our needs.
   - Text content there is licensed CC-BY 4.0. [see the footer](http://webosose.org/discover/contributing/overview/)
 - In the end [TiddlyWiki licensing](https://github.com/Jermolene/TiddlyWiki5/tree/master/licenses) will apply. 
   - ATM we use [CC-BY V3.0](https://github.com/Jermolene/TiddlyWiki5/blob/master/licenses/cla-individual.md#23-outbound-license) so it should be compatible. 
