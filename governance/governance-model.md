
# Governance Model

We encourage participation by anyone who wants to collaborate on the direction of the TiddlyWiki project. Hoping for your active involvement, we introduce our community and processes.

## Organization

TiddlyWiki.org is an open community, which is governed by the [BSD-3-Clause license](https://github.com/Jermolene/TiddlyWiki5/blob/master/license) (TODO fix link) for code and [Creative Commons - CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/) for the documentation. 

The open community is comprised of TiddlyWiki team members and external contributors. Since the community structure is evolving, in this initial stage, all components will have oversight by the TiddlyWiki dedicated development team TODO. We hope that public contributors will also actively participate and expand their role as reviewer and maintainer.

## Communication and Process

TiddlyWiki uses well-known tools and processes to make contributing and communicating straightforward.

These resources include this website, and [forums](https://gitlab.com/tiddlywiki.org/senatus/commitee/blob/master/governance/forums.md). We host code on GitLab, and maintainers use the well-known GitLab merge request mechanism to review and accept contributions. Learn more about [How We Accept Contributions](../guidelines/how-we-accept-contributions.md).

Everyone contributing to TiddlyWiki or participating in the forums or other resources provided by TiddlyWiki.org must abide by the Terms and Conditions and [Community Guidelines](../guidelines/community-guidelines.md). While creative ideas and respectful debates on topics are encouraged, TiddlyWiki reserves the right to act as needed to protect the integrity of the project, including the removal of any posts deemed inappropriate or offensive.

Community members should note that the information submitted via the forums or any other communications, resources of the project will immediately become public information.
