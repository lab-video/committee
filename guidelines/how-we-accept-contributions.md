# How We Accept Contributions

We recommend that you verify your achievement before submitting a contribution. After you are convinced, we will start reviewing your request.

## Criteria for Making a Merge Request

Check the following information before you make a merge request.

- I have verified that my changes do not break any of the builds.

- I have provided or updated unit tests if there is an existing unit test structure for any of the components affected.

- If there is no unit test structure, I have thoroughly tested my changes manually and can describe the results.

- The code is in the style of the code that surrounds it.

- I have followed the [commit guidelines]. (TODO)

## The Contribution Process

(TODO Image)

## What You Need to Do:

 1. Before submitting code, if you think your changes affect major parts of the platform, communicate with the Project via the forum to get feedback.

 2. Make a VCS merge request.

 3. Sign the Contributor License Agreement (CLA). (TODO link)
   - You can proceed with the contribution after CLA signing.
   - The CLA signing is processed once in the VCS repository upon a merge request.  
   - If the CLA is changed, CLA signing is required again.

## What Happens After You Submit a Contribution:

1. Maintainer(s) conducts a code review, verifies CLA signing, runs tests and asks for adjustments from you as necessary.

2. They will vote on the merge request with reviewers. Once the vote is approved, they will accept the merge request.

3. They merge the commits into the repository and closes the merge request.

## During Review:

- Maintainer(s) looks to see that your CLA signing is in all your commit messages, including format, and presence of the real name and the real email address.

- If you are someone entirely new to the Project, they may get in touch with you via the contact information you have provided.

- If there are anomalies such as inconsistent name or email address, they may ask you to clarify.

- This process may take some time, since we may conduct testing, and there may be concurrent activities which must be checked for merge conflicts, architectural issues, etc.

## What You will See Once Your Merge Request Has Been Reviewed and Accepted:

- The maintainer's identity who accepted your merge request will be recorded in the merge.

- The merge request will be closed.

**Congrats, your contribution is in!**

-----

If Your Existing merge request Contains Commits That Don't Have the CLA Signing:

You can fix this by the following:

- Assuming no one has forked from your fork, or you do not mind breaking them, use "rebase -i" to edit the existing commit messages. For more information, refer to git-rebase.

- Open a new merge request with the fixed commits.

- Close the existing merge request, with a comment showing the new one that supersedes it.

